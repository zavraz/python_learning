def my_map(list_funcs, vars): #объявление функции, принимающей функцию/список функций, и еще один аргумент список 
    if isinstance(list_funcs, list): # если передается список функций
        return [tuple(func(var) for var in vars) for func in list_funcs] # функции будут вызываться последовательно, результат выполнения функции для переданного списка переводится в кортеж
    else:
        return [list_funcs(var) for var in vars] # иначе был передан не список функций, а одна функция

def addition(num_1): # объявление функции-фабрики с обычной функцией внутри
    def add(num_2): # внутренняя функция
        return num_1 + num_2 # возвращение суммы
    return add # возвращение внутренней функции суммирования, но не ее вызов
def addition_range(left, right): # установка границ диапазона
        return [addition(x) for x in range(left, right)] # возвращение списка из функций сложения
list_of_func = addition_range(0, 5) # получение ссылки на список функций сложения в заданном диапазоне
print my_map(list_of_func, [1, 2, 3]) #выхываем функцию-аналог map