# -*- coding: utf-8 -*-
"""Функция-генератор chain, которая последовательно
итерирует переданные объекты (произвольное количество)
>>> i1 = iter([1, 2, 3])
>>> i2 = iter([4, 5])
>>> c = chain(i1, i2)
>>> c.next()
1
>>> c.next()
2
>>> c.next()
3
>>> c.next()
4
>>> c.next()
5
>>> c.next()
Traceback (most recent call last):
  ...
StopIteration
"""
def chain(*args):
    """Функция-генератор"""
    for arg in args:
        for elem in arg:
            yield elem
i1 = iter([1, 2, 3])
i2 = iter([4, 5])
c = chain(i1, i2)
for y in range(5):
    print c.next()
if __name__ == '__main__':
# test myself
    import doctest
    doctest.testmod()
