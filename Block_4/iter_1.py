# -*- coding: utf-8 -*-
"""Функция-генератор, возвращающая
циклический итератор
>>> i = iter([1, 2, 3])
>>> c = cycle(i)
>>> c.next()
1
>>> c.next()
2
>>> c.next()
3
>>> c.next()
1
"""
import itertools
def cycle(list):
    """Функция-генератор"""
    for element in itertools.cycle(list):
        yield element
i = iter([1, 2, 3])
#с - итератор генератора
c = cycle(i)
for y in range(7):
    print c.next()
if __name__ == '__main__':
# test myself
    import doctest
    doctest.testmod()
