lst = [1,5,4,3] # creating the list
i=0 # var for counter
for x in lst[:]: # for every element of list; [:] is responsible for copy of lst. For instance, if another thread changes lst, for statement will be able to continue its work
    lst[i] = x ** 2 # squaring
    i = i + 1 # step of iteration
print lst # printing the result