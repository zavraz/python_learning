list_1 = ["key_1", "key_2", "key_3", "key_4"]
list_2 = [1,2,3] # 2nd list
def func(m_list_1, m_list_2):
    m_dict = {}
    m_list_2 += [None] * (len(m_list_1) - len(m_list_2))
    for i in range(len(m_list_1)):
        m_dict[m_list_1[i]] = m_list_2[i]
    return m_dict
print func(list_1, list_2)