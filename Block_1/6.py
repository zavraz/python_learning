first_tuple = ('z','a','v', 'p') # 1st tuple
second_tuple = ('p','i','s') # 2nd tuple
print tuple(set(first_tuple + second_tuple)) # since elements of a set are never duplicated, all repeated elements will be deleted