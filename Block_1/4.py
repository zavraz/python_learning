y=5
help(y) # command to get information about particular object, module, function
print y.__doc__ # __doc__ attribute prints doc strings
help() # command to obtain general information