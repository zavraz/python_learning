# -*- coding: utf-8 -*-
"""Написать класс, который бы по всем внешним признакам был бы словарем,
но позволял обращаться к ключам как к атрибутам."""
class DictAttr(dict):
    """Класс яв-ся наследником словаря, перепишем геттеры и сеттеры
    >>> x = DictAttr([('one', 1), ('two', 2), ('three', 3)])
    >>> x
    { 'one': 1, 'three': 3, 'two': 2}
    >>> x['three']
    3
    >>> x.get('one')
    1
    >>> x.get('five', 'missing')
    'missing'
    >>> x.one
    1
    >>> x.five
    Traceback (most recent call last):
    ...
    AttributeError
    class Observable(object):
    """
    def __setattr__(self, key, val):
        self[key] = val
    def __getattr__(self, key):
        try:
            return self[key]
        except KeyError:
            raise AttributeError

x = DictAttr([('one', 1), ('two', 2), ('three', 3)])
print x.get('five', 'missing')
print x.get('two')
print x.six
