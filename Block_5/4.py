# -*- coding: utf-8 -*-
"""
Написать класс, который регистрирует свои экземпляры
и предоставляет интерфейс итератора по ним
"""
class IterReg(type):
    def __iter__(self):
        return iter(self.instances)

class Reg(object):
    """
    >>> x = Reg()
    >>> x
    <Reg instance at 0x98b6ecc>
    >>> y = Reg()
    >>> y
    <Reg instance at 0x98b6fec>
    >>> z = Reg()
    <Reg instance at 0x98ba02c>
    >>> for i in Reg:
    ...     print i
    <Reg instance at 0x98b6ecc>
    <Reg instance at 0x98b6fec>
    <Reg instance at 0x98ba02c>
    """
    __metaclass__ = IterReg
    instances = []
    def __init__(self):
        self.__class__.instances.append(self)
x = Reg()
y = Reg()
print x
for i in Reg:
    print i