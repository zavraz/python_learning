# -*- coding: utf-8 -*-
"""1. Написать базовый класс Observable, который бы позволял наследникам:
a. при передаче **kwargs заносить соответствующие значения как атрибуты
xb. сделать так, чтобы при print отображались все публичные атрибуты"""
class Observable(object):
    """
    >>> x = X(foo=1, bar=5, _bazz=12, name='Amok', props=('One', 'two'))
    >>> print x
    X(bar=5, foo=1, name='Amok', props=('One', 'two'))
    >>> x.foo
    1
    >>> x.name
    'Amok'
    >>> x._bazz
    12
    """
    def __init__(self, **kwargs):
        """При инициализации занесем переданные аргументы"""
        for key in kwargs.keys():
            setattr(self, key, kwargs[key])
    def __str__(self):
        """__str__ - Возвращает строковое представление объекта"""
        line = self.__class__.__name__ +'('
        #сформируем строку из пользовательских атрибутов, исключая печать скрытых
        for key, val in self.__dict__.iteritems():
            if not key.startswith('_'):
                line += str(key) + '=' + str(val) + ', '
        line += ')'
        line = line.replace(', )', ')')
        return line

class X(Observable):
    pass

x = X(foo=1, bar=5, _bazz=12, name='Amok', props=('One', 'two'))
print x