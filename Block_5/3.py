# -*- coding: utf-8 -*-
"""
Пункт 2 с усложнением: написать родительский класс XDictAttr так,
чтобы у наследника динамически определялся ключ по наличию метода get_<KEY>.
"""
class DictAttr(dict):
    """Класс яв-ся наследником словаря, задача 2"""
    def __setattr__(self, key, val):
        self[key] = val
    def __getattr__(self, key):
        try:
            return self[key]
        except KeyError:
            raise AttributeError

class XDictAttr(DictAttr):
    "Класс XDictAttr"
    def __myget__(self, key):
        method = 'get_' + key
        if callable(getattr(self, method)):
            return getattr(self, method)
        else: return False

    def __getitem__(self, key):
        myget = self.__myget__(key)
        if myget:
            return myget()
        return super(XDictAttr, self).__getitem__(key)

    def __getattr__(self, key):
        myget = self.__myget__(key)
        if myget:
            return myget()
        return super(XDictAttr, self).__getattr__(key)

    def get(self, key, default=None):
        myget = self.__myget__(key)
        if myget:
            return myget()
        return super(XDictAttr, self).get(key, default)


class X(XDictAttr):
    """
    >>> x = X({'one': 1, 'two': 2, 'three': 3})
    >>> x
    X: { 'one': 1, 'three': 3, 'two': 2}
    >>> x['one']
    1
    >>> x.three
    3
    >>> x.bar
    12
    >>> x['foo']
    5
    >>> x.get('foo', 'missing')
    5
    >>> x.get('bzz', 'missing')
    'missing'
    """
    def get_foo(self):
        return 5
    def get_bar(self):
        return 12
x = X({'one': 1, 'two': 2, 'three': 3})
print x
print x['foo']