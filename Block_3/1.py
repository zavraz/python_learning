def my_decorator(incoming_doc_str):
    "Декоратор оставляет о себе напоминанение в docstring что он там был"
    def wrapper(*args, **kwargs):
        wrapper.__doc__ = incoming_doc_str.__doc__ + "\nТут был декоратор"
        res = incoming_doc_str()
        return res
    return wrapper
#синтаксический сахар, эквивалент doc_str_func = my_decorator(doc_str_func)
@my_decorator 
def doc_str_func():
    "Декорируемая функция ничего не возвращает"
doc_str_func()
print doc_str_func.__doc__