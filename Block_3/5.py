list_cash = {}
def dec_with_arg(cash):
    def my_decorator(incoming_func):
        "Декоратор выполняет кэширование декорируемой функции"
        def wrapper(*args):
            if args in cash:
                print "Значение взято из кэша. Квадрат", int(*args), "равен", cash[args]
                return cash[args]
            else:
                result = incoming_func(*args)
                cash[args] = result
                return result
        return wrapper
    return my_decorator
@dec_with_arg(list_cash)
def func(arg):
    "Функция возвращает квадрат переданного аргумента"
    res = arg**2
    print "Квадрат", arg, "равен", res
    return res
func(5)
func(6)
func(5)
print list_cash