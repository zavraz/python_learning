import time
def my_decorator(incoming_func):
    "Декоратор вычисляет время выполнения декорируемой функции"
    def wrapper(*args, **kwargs):
        t = time.time()
        result = incoming_func(*args, **kwargs)
        print "Время выполнения функции:", time.time() - t, "с"
        return result
    return wrapper
@my_decorator
def func(arg_1, arg_2):
    "Функция возвращает сумму двух слагаемых"
    res = arg_1 + arg_2
    print "Сумма", arg_1, "и", arg_2, "равняется", res
    return res
func(5, 2)