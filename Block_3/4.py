def decorator_with_arg(*dargs):
    def my_decorator(incoming_func):
        """Декоратор принимает в себя набор типов и проверяет соответствие 
        выходныx данных декорируемой функции"""
        def wrapper(*args):
            result = incoming_func(*args)
            set_incoming_fung_arg = set()
            set_incoming_fung_arg.update(map(type, result))
            if set_incoming_fung_arg.issubset(set(dargs)): print "Типы соответствуют"
            else: print "Обнаружено несоответствие типов"
            return result
        return wrapper
    return my_decorator
@decorator_with_arg(int, float)
def func(arg_1, arg_2):
    print "Аргумент 1:", arg_1
    print "Аргумент 2:", arg_2
    return arg_1, arg_2
func(2, 2)