def decorator_with_arg(*dargs):
    def my_decorator(incoming_func):
        """Декоратор принимает в себя набор типов и проверяет соответствие 
        входныx данных декорируемой функции"""
        def wrapper(*args):
            set_incoming_fung_arg = set()
            set_incoming_fung_arg.update(map(type, args))
            if set_incoming_fung_arg.issubset(set(dargs)): print "Типы соответствуют"
            else: print "Обнаружено несоответствие типов"
            result = incoming_func(*args)
            return result
        return wrapper
    return my_decorator
@decorator_with_arg(int, float)
def func(arg_1, arg_2):
    print "Аргумент 1:", arg_1
    print "Аргумент 2:", arg_2
func(2, 2)